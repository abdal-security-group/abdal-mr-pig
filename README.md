# Abdal Mr Pig
**[![Download Abdal Mr Pig](https://img.shields.io/sourceforge/dt/abdal-mr-pig.svg)](https://sourceforge.net/projects/abdal-mr-pig/files/latest/download)**

[![Download Abdal Mr Pig](https://a.fsdn.com/con/app/sf-download-button)](https://sourceforge.net/projects/abdal-mr-pig/files/latest/download)
## Project Programmer
> Ebrahim Shafiei (EbraSha) - Prof.Shafiei@Gmail.com

## Screenshot

![](abdal-mr-pig-s.jpg)
![](https://github.com/abdal-security-group/abdal-mr-pig/blob/main/img/abdal-mr-pig-s.jpg)

## Made For

Secure file transfer with SSH


**Requires**
> Visual Studio 2019 - Telerik WinForm - Chilkat - .NetFramework 4.* - nSoftware SecureBox
>


Features

- Upload
- Download
- Delete File Or Folder
- Support publicKey And TrustKey
- No malware
- Open Source
- Very high speed

## ❤️ Donation
> USDT:      TXLasexoQTjKMoWarikkfYRYWWXtbaVadB

> bitcoin:   19LroTSwWcEBY2XjvgP6X4d6ECZ17U2XsK

> For Iranian People -> MellatBank : 6104-3378-5301-4247


## Reporting Issues

If you are facing a configuration issue or something is not working as you expected to be, please use the **Abdal.Group@Gmail.Com** or **Prof.Shafiei@Gmail.com** . Issues on GitLab are also welcomed.
